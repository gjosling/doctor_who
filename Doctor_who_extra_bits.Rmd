---
title: "Doctor_who_extra_bits"
author: "Gabrielle Josling"
date: "2/3/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

We can also visualise the differences between characters using correspondence analysis

#### Cluster analysis
```{r}
top_20  <- dt_scripts[ , .N , by = character][order(-N)][1:20, character]
rare_words <- dfm_all %>% 
    dfm_group(groups = "episode_num") %>% 
    dfm_trim(max_docfreq = 1) %>% 
    featnames()

char_ca <- dfm_all %>%
    dfm_remove(rare_words) %>% 
    dfm_subset(character %in% top_20 & character != "COMPUTER") %>% 
    dfm_group(groups = "character") %>% 
    #dfm_trim(min_termfreq = 0.8, termfreq_type = "quantile", 
    #         max_docfreq = 0.2, docfreq_type = "prop") %>% 
    textmodel_ca(sparse = TRUE)

factoextra::get_eigenvalue(char_ca)

dat_ca <- data.frame(dim1 = coef(char_ca, doc_dim = 1)$coef_document, 
                      dim2 = coef(char_ca, doc_dim = 2)$coef_document,
                     character = char_ca$rownames)

dat_ca2 <- data.frame(dim1 = coef(char_ca, feat_dim = 1)$coef_feature,
                      dim2 = coef(char_ca, feat_dim = 2)$coef_feature,
                    token = char_ca$colnames)
library(ggplot2)
library(ggrepel)
ggplot(dat_ca) +
    geom_point(aes(dim1, dim2, colour = character)) +
    geom_text_repel(aes(dim1, dim2, label = character))

ggplot(dat_ca2) +
    geom_text(aes(dim1, dim2, label = token))
```
Identify words closest to each character?
Filter out episode specific words?

Interestingly, here we see that the characters basically cluster by timing, with companions generally  being closest to the doctor they knew. It's interesting that this result is quite differnt from the dendrogram. 

More explanation here. Maybe because  still names remaining? Cluster episodes?


### Sentiment analysis
```{r}
library(sentimentr)

companions <- c("rose", "martha", "donna", "amy", "rory", "clara",
                "bill","yasmin","ryan", "graham")

dt_sentiment <- with(dt_scripts, 
                     sentiment_by(
                         get_sentences(dialogue), 
                         list(character, episode_num, episode)))

dt_doctors <- dt_sentiment[character %in% paste("DOCTOR", 9:13)]
dt_companions <- dt_sentiment[character %in% toupper(companions)]

ggplot(dt_doctors) +
    geom_point(aes(episode_num, ave_sentiment, colour = character)) +
    scale_colour_viridis_d()
    

ggplot(dt_companions) +
    geom_point(aes(episode_num, ave_sentiment, colour = character)) +
    scale_colour_viridis_d() +
  facet_wrap(~ character )
```
When characters appear out of time, sentiment is often unusually high or low.
Point out high and low points

Sentiment over each episode
```{r}
dt_scripts[,line := rowid(episode)]

dt_sentiment_all <- with(dt_scripts, 
                     sentiment_by(
                         get_sentences(dialogue), 
                         list(episode, episode_num, character, line)))

dt_sentiment_all[, total := max(line), by = episode][, prop := line / total]

ggplot(dt_sentiment_all[1:10000]) +
  geom_point(aes(prop, ave_sentiment, colour = episode)) +
  geom_smooth(aes(prop, ave_sentiment)) +
  facet_wrap(~episode) +
  theme(legend.position = "none")

```

```{r}
dt_top <- dt_sentiment[character %in% top_30]
ggplot(dt_top) +
    geom_point(aes(episode_num, ave_sentiment, colour = character)) +
    scale_color_viridis_d()

ggplot(dt_top) +
    geom_violin(aes(character, ave_sentiment))
```

Profanity
```{r}
dt_profanity <- with(dt_scripts, 
                     profanity_by(
                         get_sentences(dialogue), 
                         list(character, episode_num, episode)))

dt_doctors_p <- dt_profanity[character %in% paste("DOCTOR", 9:13)]
dt_companions_p <- dt_profanity[character %in% toupper(companions)]

ggplot(dt_doctors_p) +
    geom_point(aes(episode_num, ave_profanity, colour = character)) +
    scale_colour_viridis_d()
    

ggplot(dt_companions_p) +
    geom_point(aes(episode_num, ave_profanity, colour = character)) +
    scale_colour_viridis_d() 


```



### Catchphrases over time

```{r}
phrases <- c("bow_ties", "bow_tie", "allons_y", "geronimo", "fantastic", "souffle", "raggedy")
dfm_phrase <- dfm_ngrams %>% 
    dfm_match(phrases) %>% 
    dfm_group("episode_num")

dt_phrase <- convert(dfm_phrase, "data.frame")
dt_phrase$bow_tie <- dt_phrase$bow_tie + dt_phrase$bow_ties
dt_phrase$bow_ties <- NULL
dt_phrase <- as.matrix(dt_phrase[,2:6])
heatmap(dt_phrase, Rowv = NA)
```
Colour black and white

Compare doctors (wordcloud)

```{r}

dfm_doctor <- dfm_all %>% 
    #dfm_remove(names) %>% 
    dfm_subset(character %in% paste("DOCTOR", seq(9, 13))) %>% 
    dfm_group(groups = "character")

textplot_wordcloud(dfm_doctor, max_words = 150, comparison = TRUE)

```
Not that interesting

### Comparing top characters
This analysis  gives us some sense of the differences between the top characters, but we can do a lot more.

```{r}
library(ggdendro)
tstat_dist <- as.dist(textstat_dist(dfm_top))
clust <- hclust(tstat_dist, method = "ward.D") # look into different methods
plot(clust, xlab = "Distance", ylab = NULL)
dendro <- dendro_data(clust, type = "rectangle")
ggplot() +
    geom_segment(data = segment(dendro), aes(x = x, y = y, xend = xend, yend = yend)) +
    geom_text(data = label(dendro), aes(x, y, label = label), hjust = 1.2, angle = 90) +
    theme_dendro() +
    theme(axis.text.x = element_blank()) +
    scale_y_continuous(expand = expand_scale(mult = 1))
```

This is dendrogram showing the distances (calculated using `textstat_dist`) between characters. Doctors 10, 11, and 12 cluster together distinctly from the other characters, indicating they are most similar to each other. Doctor 9 clusters with many of the companions, and Clara, Amy, and Rose form their own distinct cluster.

More characters?

```{r}
episode_ca <- dfm_all %>%
    dfm_remove(names) %>% 
    dfm_subset(character %in% top_20 & character != "COMPUTER") %>% 
    dfm_group(groups = "episode_num") %>% 
    dfm_trim(min_docfreq = 2) %>% 
    textmodel_ca(sparse = TRUE)

dat_episode_ca <- data.frame(dim1 = coef(episode_ca, doc_dim = 1)$coef_document, 
                      dim2 = coef(episode_ca, doc_dim = 2)$coef_document,
                     episode_num = episode_ca$rownames)
library(ggplot2)
library(ggrepel)
ggplot(dat_episode_ca) +
    geom_point(aes(dim1, dim2, colour = episode_num)) +
    theme(legend.position = 'none') +
    scale_colour_viridis_d()

```


geom_ribbon to shade by doctor.
